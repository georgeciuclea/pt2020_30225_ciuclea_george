package com.asgn1;

@SuppressWarnings("unused")
public class Monom {
    float coeficient;
    int exponent;
    public Monom()
    {
        this.coeficient=0;
        this.exponent=0;
    }
    public Monom(float coeficient,int exponent)
    {
        this.coeficient=coeficient;
        this.exponent=exponent;
    }
    public void add(Monom m2)
    {
        if(this.exponent!=m2.exponent)
            return;
        this.coeficient+=m2.coeficient;
    }
    public void sub(Monom m2)
    {
        if(this.exponent!=m2.exponent)
            return;
        this.coeficient-=m2.coeficient;
    }
    public Monom neg()
    {
        return new Monom(-this.coeficient,this.exponent);
    }
}
