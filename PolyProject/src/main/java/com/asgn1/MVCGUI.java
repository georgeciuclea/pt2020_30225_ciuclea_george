package com.asgn1;

import java.awt.*;
import java.awt.event.ActionListener;

class MVCGUI extends javax.swing.JFrame implements ActionListener {
    /*
    Constructor standard, pregateste clasa extinsa JFrame pentru afisare
     */
    public MVCGUI() {
        initComponents();//initializeaza partea de View
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName()); //seteaza "tema" windows
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MVCGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        this.setLocationRelativeTo(null);//centreaza pe ecran fereastra noua
    }
    //View
    private void initComponents() {

        p1=new Polinom();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        javax.swing.JScrollPane jScrollPane1 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        javax.swing.JLabel jLabel1 = new javax.swing.JLabel();
        javax.swing.JLabel jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        jButton1.addActionListener(this);
        jButton2.addActionListener(this);
        jButton3.addActionListener(this);
        jButton4.addActionListener(this);

        setResizable(false);

        jButton1.setText("Inmultire");

        jTextPane1.setEditable(false);
        jTextPane1.setContentType("text/html");
        jTextPane1.setText("");
        jScrollPane1.setViewportView(jTextPane1);

        jButton2.setText("Impartire");

        jButton3.setText("Derivare");

        jButton4.setText("Integrare");

        jLabel1.setFont(new java.awt.Font("Tahoma", Font.PLAIN, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel1.setText("Polinom nou:");

        jLabel2.setFont(new java.awt.Font("Tahoma", Font.PLAIN, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel2.setText("Rezultat:");

        //am folosit layout bazat pe grupuri orizontale si verticale
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(jLabel2)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jScrollPane1)
                                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 700, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jButton1)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(layout.createSequentialGroup()
                                                        .addComponent(jButton2)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(jButton3)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(jButton4))
                                                .addComponent(jScrollPane1))
                                        .addComponent(jLabel2))
                                .addContainerGap())
        );

        pack();
    }

    //Model
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JTextField jTextField1;
    private Polinom p1;


    //Controller
    public void actionPerformed(java.awt.event.ActionEvent evt) {
        if (evt.getSource() == jButton1) {
            MVCGUI.this.btnInmultire();
        }
        else if (evt.getSource() == jButton2) {
            MVCGUI.this.btnImpartire();
        }
        else if (evt.getSource() == jButton4) {
            MVCGUI.this.btnIntegrare();
        }
        else if (evt.getSource() == jButton3) {
            MVCGUI.this.btnDerivare();
        }
    }
    //functii event
    private void btnInmultire() {
        if(jTextField1.getText().replace(' ','\0').length()<1)
        {
            return;
        }
        Polinom p2=new Polinom(jTextField1.getText());
        p1.inmultire(p2);
        jTextPane1.setText("<html>"+p1.print()+"</html>");
    }
    private void btnImpartire() {
        if(jTextField1.getText().replace(' ','\0').length()<1)
        {
            return;
        }
        Polinom p2=new Polinom(jTextField1.getText());
        Polinom[] prez;
        prez=p1.impartire(p2);
        jTextPane1.setText("<html>"+p1.print()+" / "+p2.print()+"<br>"+prez[0].print()+"<br>"+prez[1].print()+"</html>");
        p1=prez[0];
    }
    private void btnIntegrare() {
        if(jTextField1.getText().replace(' ','\0').length()<1)
        {
            return;
        }
        int i;
        i=Integer.parseInt(jTextField1.getText());
        p1.inmultire(new Polinom(new Monom(1,1)));
        p1.add(new Monom(i,0));
        jTextPane1.setText("<html>"+p1.print()+"</html>");
    }
    private void btnDerivare() {
        Polinom p2=new Polinom(new Monom(1,1));
        p1=p1.impartire(p2)[0];
        jTextPane1.setText("<html>"+p1.print()+"</html>");
    }
}
